# Ansible Role: Fail2ban

[![pipeline status](https://gitlab.com/barlebenx/ansible-role-fail2ban/badges/main/pipeline.svg)](https://gitlab.com/barlebenx/ansible-role-fail2ban/-/commits/main)

Basic install of fail2ban on debian.

## Requirements

- debian 9 or 10

## Role Variables

| name                    | default                                                         | description                                                       |
|-------------------------|-----------------------------------------------------------------|-------------------------------------------------------------------|
| fail2ban_ignored_ips    | []                                                              |                                                                   |
| fail2ban_bantime        | 1296000                                                         | 15 days                                                           |
| fail2ban_findtime       | 86400                                                           | 1 day                                                             |
| fail2ban_maxretry       | 3                                                               |                                                                   |

## Dependencies

## Example playbook

```yaml
- hosts: all
  roles:
    - role: fail2ban
```
